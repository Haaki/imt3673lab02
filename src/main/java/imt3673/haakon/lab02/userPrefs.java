package imt3673.haakon.lab02;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class userPrefs extends AppCompatActivity {
    SharedPreferences sp;
    private EditText mEditText;
    private Spinner mRefreshRate;
    private Spinner mListEntries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_prefs);
        sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE);

        mEditText = findViewById(R.id.rssFeedSource);
        mRefreshRate = findViewById(R.id.refreshRate);
        mListEntries = findViewById(R.id.listEntries);

        mEditText.setText(sp.getString("your_rss_source",""));
        ArrayAdapter<CharSequence> refreshRateAdapter = ArrayAdapter.createFromResource(this, R.array.refresh_rate_array, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> listEntriesAdapter = ArrayAdapter.createFromResource(this, R.array.list_entries_array, android.R.layout.simple_spinner_item);

        refreshRateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        listEntriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mRefreshRate.setAdapter(refreshRateAdapter);
        mListEntries.setAdapter(listEntriesAdapter);
        mRefreshRate.setSelection(sp.getInt("your_refresh_rate", 0));
        mListEntries.setSelection(sp.getInt("your_list_entries", 0));
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("your_rss_source", mEditText.getText().toString());
        editor.putInt("your_refresh_rate", mRefreshRate.getSelectedItemPosition());
        editor.putInt("your_list_entries", mListEntries.getSelectedItemPosition());
        int aRefreshRate = 1;
        switch (mRefreshRate.getSelectedItemPosition()){
            case 0: aRefreshRate = 10; break;
            case 1: aRefreshRate = 60; break;
            case 2: aRefreshRate = 1440; break;
        }
        editor.putInt("actual_refresh_rate", aRefreshRate * 60 * 1000);
        editor.putInt("actual_list_entries", Integer.parseInt(mListEntries.getSelectedItem().toString()));
        editor.apply();
    }
}
