package imt3673.haakon.lab02;

public class RssFeedModel {

    public String title;
    public String link;
    public String description;
    public String date;

    public RssFeedModel(String title, String link, String description, String date) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.date = date;
    }
}